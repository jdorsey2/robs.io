<!doctype html>
<html lang="en">
<head>
    <title>Choose Interfaces Over Structs in Golang</title>
    <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate"/>
  <link rel="shortcut icon" href="/assets/img/logo.png" type="image/png">
  <meta http-equiv="Pragma" content="no-cache"/>
  <meta http-equiv="Expires" content="0"/>
  <link rel="shortcut icon" href="/assets/img/logo.png" type="image/png">
  <link rel="stylesheet" href="/assets/styles.css">
  <link rel="stylesheet" href="styles.css">
</head>
<body>
<header class=nav-bar>
  <nav>
    <div class=sidebar-button>≡</div>
    <a class="homelink spy" href="/">r<span class=hideable>obs.io</span></a>
    <div class="nav-right">
      <!-- div id=search-box></div -->
      <ul class=nav-links>
        <li class=nav-item><a class=nav-link href="/wish/">Wishes</a></li>
        <li class=nav-item><a class=nav-link href="/quotes/">Quotes</a></li>
        <li class=nav-item><a class=nav-link href="/really/">Really?</a></li>
        <li class=nav-item><a class="nav-link" href="https://rwx.gg">rwx.gg</a></li>
        <li class=nav-item><a class="nav-link" href="https://youtube.com/rwxrob/">YouTube</a></li>
        <li class=nav-item><a class="nav-link" href="https://gitlab.com/rwxrob/">GitLab</a></li>
        <li class=nav-item><a class="nav-link" href="https://linkedin.com/in/rwxrob">LinkedIn</a></li>
        <li class=nav-item><a class="nav-link" href="https://twitter/rwxrob">Twitter</a></li>
      </ul>
    </div>
  </nav>
</header>
<main id=top>
<div class=container>  <h1 id=title class=duck><a href="https://duckduckgo.com/lite?kae=t&q=Choose Interfaces Over Structs in Golang">Choose Interfaces Over Structs in Golang</a></h1>
<p>These days it’s pretty common knowledge that creating anything in Golang as a struct is a bad idea unless you <em>really</em> have a good reason. That’s because interfaces are so much more flexible and surprisingly often faster than structs. Here are a few examples.</p>
<h2 id="easier-to-document">Easier to Document</h2>
<p>It’s a small but significant advantage to be able to document each attribute of thing individually instead of jamming a ton of notes that make a struct definition rather hard to read. Let’s take the example of a thing we’ll call <code>Entry</code>, as in an entry to a log or list. As a struct it might look like this:</p>
<pre class="go"><code>type Entry struct {
  Tstamp time.Time
  Amount float64
  Note string
}</code></pre>
<p>To document that you have three choices:</p>
<ol type="1">
<li>Add a bunch of trailing comments</li>
<li>Add preceding line comments</li>
<li>Add some sections to the Entry comment</li>
</ol>
<p>I’ll just let you imagine what that looks like, but believe me, none of them are really good options. The trailing comments get in the way of <code>json</code> and <code>yaml</code> tags. The preceding line comments break the structure up so seeing the overall structure of the structure is difficult and changing to exported is problematic. The section in the Entry comments is removed from the <code>struct</code> field so that you forget to remove or update it when you change something. No one is going to die if you use one of these methods, and quite frankly you <em>have</em> to pick one when documenting a <code>struct</code> at all. The standard library is full of examples.</p>
<p>But let’s consider what happens when you use an <code>interface</code> instead:</p>
<pre class="go"><code>
type Tstamp interface {
  Tstamp time.Time
}

type Amount interface {
  Amount float64
}

type Note interface {
  Note string
}

type Entry interface {
  Tstamp
  Amount
  Note
}</code></pre>
<p>Sure the code gets <em>way</em> longer, but that is not a bad thing in the long run. It provides an obvious way to document each interface fully. Not only can each interface be documented by itself, but the inline comments on the interface methods will never have to contend with tags for room at the end of a line.</p>
<p>Of course, this also allows other combinations of these fields to be made into other <code>interfaces</code>. This is composition at its finest.</p>
<h2 id="pass-by-pointer-or-value-neither-its-an-interface">Pass by Pointer or Value? Neither It’s an Interface</h2>
<p>When dealing with interfaces you also don’t have to make that painful decision about accepting a pointer or copy of a thing into your functions and methods. Anything that fulfills an interface can simply use the name of that interface.</p>
<h2 id="flexibility">Flexibility</h2>
<p>The above example with <code>Entry</code> works for any of the following function calls:</p>
<pre class="go"><code>func PrintTstamp(t Tstamp){fmt.Println(t)}
func PrintAmount(a Amount){fmt.Println(a)}
func PrintNote(n Note){fmt.Println(n)}

type entry struct {
  t time.Time
  a float64
  n string
}

func (e *entry) Tstamp() time.Time {return e.t}
func (e *entry) Amount() float64 {return e.a}
func (e *entry) Note() string {return e.n}

func main() {
  ent := entry{time.Now(),23.3,&quot;Just a test&quot;}
  PrintTstamp(ent)
  PrintAmount(ent)
  PrintNote(ent)
}</code></pre>
<p>The time spent writing out this out using the interfaces can easily be made back later by allowing your code to accept the same argument for multiple different functions and methods that would otherwise force you to only allow a single structure to be passed. When those conflicts happen you have to do gymnastics to extract out the data needed and create an entirely new struct just to comply with a poorly designed function that is not written to accept arguments that are interfaces instead. In fact, <em>thou shalt not write functions and methods that accept structs</em> unless you <em>really</em> want to hate yourself later. Don’t believe me? Spend an afternoon looking at all the ways this has been done very well in the Go standard library.</p>
<h2 id="when-data-has-already-been-parsed-or-loaded">When Data Has Already Been Parsed or Loaded</h2>
<p>Very often there are already low-level libraries for parsing everything you need up into structs of data. Such is the case, for example, with <code>goldmark</code>. It parses rather complicated CommonMark — including the YAML data — into a full abstract syntax tree. The YAML data — even if you have multiple YAML data blocks throughout your CommonMark — is all combined into a single, wonderful <code>map[string]interface{}</code>.</p>
<p>Now consider the problems and inefficiencies of defining your data model using <code>struct</code> instead of <code>interface</code>. Here’s a <code>Person</code> <code>struct</code>:</p>
<pre class="go"><code>type Person struct {
  Name string
  Age int
}</code></pre>
<p>In order to use that you have to write some sort of code that would <em>copy over the data</em> from the <code>map[string]interface{}</code>. Because you are using primitive data you can’t really use pointers and if you did it would negate the other advantages of having a struct for marshaling and such. But you don’t need built in Go unmarshaling at all and probably don’t want it because of how finicky it can get and silently just fail to unmarshal properly because some data didn’t match. No, we are going to use <code>goldmark</code> to do all that heavy lifting for us.</p>
<p>Besides, we are talking about <em>reading</em> data that is maintained in CommonMark and YAML. That means that any requirement to change any of it through any coding interface is just a bad idea. It would be better to leverage a good raw CommonMark editor than try to build a GUI on top of that. Because we are just reading from parsed data all we really need are methods that return the data that we want. Here’s the same <code>Person</code> as an <code>interface</code> instead:</p>
<pre class="go"><code>type Person interface {
  Name() string
  Age() int
}</code></pre>
<p>It might seem counter-intuitive, but <code>interface</code> method calls are actually about the same speed as returning the raw struct data. This is because only a pointer to that data is required to return, such as with many of the <code>FileInfo</code> fields:</p>
<pre class="go"><code>type FileInfo interface {
    Name() string       // base name of the file
    Size() int64        // length in bytes for regular files; system-dependent for others
    Mode() FileMode     // file mode bits
    ModTime() time.Time // modification time
    IsDir() bool        // abbreviation for Mode().IsDir()
    Sys() interface{}   // underlying data source (can return nil)
}</code></pre>
<p>Look familiar? There’s a reason this is an <code>interface</code> and not a <code>struct</code>. You guessed it. It’s the same reason you just saw. If an <code>interface</code> is good enough for something as frequently accessed as <code>FileInfo</code> you can be sure it is plenty fast enough for your applications.</p>
<h2 id="full-benchmark-test-comparison">Full Benchmark Test Comparison</h2>
<p>Thanks to Go’s great built-in benchmark testing we can see all of this in action rather easily. We’ll create three alternatives:</p>
<ol type="1">
<li>Using just a <code>struct</code></li>
<li>Using an interface where the values are returned directly</li>
<li>Using an interface where the values are indirectly returned</li>
</ol>
<p><em>Or you can <a href="./lib.go">download it.</a></em></p>
<pre class="go"><code>package lib

// -------------------------------------------------

type Person struct {
    Name string
    Age  int
}

func NewPerson() {
    person := Person{Name: &quot;Rob&quot;, Age: 52}
    if person.Name == &quot;Rob&quot; {
    }
    if person.Age == 42 {
    }
}

// -------------------------------------------------

type PersonI interface {
    Name() string
    Age() int
}

type aperson struct {
    // fair to leave out since usually pointing to other data
}

func (p *aperson) Name() string {
    return &quot;Rob&quot;
}

func (p *aperson) Age() int {
    return 42
}

func NewPersonI() {
    person := new(aperson)
    if person.Name() == &quot;Rob&quot; {
    }
    if person.Age() == 42 {
    }
}

// -------------------------------------------------

type PersonII interface {
    Name() string
    Age() int
}

type aaperson struct {
    name string
    age  int
}

func (p *aaperson) Name() string {
    return p.name
}

func (p *aaperson) Age() int {
    return p.age
}

func NewPersonII() {
    person := aaperson{name: &quot;Rob&quot;, age: 42}
    if person.Name() == &quot;Rob&quot; {
    }
    if person.Age() == 42 {
    }
}</code></pre>
<p>Here’s what the benchmark (<a href="./lib_test.go"><code>lib_test.go</code></a>) code looks like:</p>
<pre class="go"><code>package lib

import &quot;testing&quot;

func BenchmarkNewPerson(b *testing.B) {
    for i := 0; i &lt; b.N; i++ {
        NewPerson()
    }
}

func BenchmarkNewPersonI(b *testing.B) {
    for i := 0; i &lt; b.N; i++ {
        NewPersonI()
    }
}

func BenchmarkNewPersonII(b *testing.B) {
    for i := 0; i &lt; b.N; i++ {
        NewPersonII()
    }
}</code></pre>
<p>And finally the results:</p>
<pre><code>goos: linux goarch: amd64
BenchmarkNewPerson-8      853628504   1.41 ns/op
BenchmarkNewPersonI-8     1000000000  0.412 ns/op
BenchmarkNewPersonII-8    873169317   1.39 ns/op</code></pre>
<p>As you can see the difference to call via <code>interface</code> methods is 0.02 ns/op. That’s pretty much the dictionary definition of “negligible”. If that kind of time difference is a problem then you are probably coding in the wrong language to begin with (try C or Assembly instead).</p>
<p>What is even more interesting is the ridiculously faster times when the value simply has to be returned and not looked up. Such is often the case when the thing is <em>already</em> there and just needs a pointer returned. This is why <code>FileInfo</code> using <code>interface</code> methods instead of having to copy over that same data into a <code>struct</code> and then return that, or provide a <code>struct</code> that contains pointers to rather primitive data. This means that, depending on how much data is just being pointed at, you could end up with even faster times matching or potentially beating that of <code>structs</code>.</p>
<h2 id="moral-of-the-story">Moral of the Story</h2>
<p>The difference in execution speed between a thing designed as a <code>struct</code> and another as an <code>interface</code> with accessor methods is negligible. While an <code>interface</code> requires a bit more code to write it buys you <em>a lot</em> of flexibility later. We’re talking the kind of flexibility that made <code>goldmark</code> dominate so completely over the <code>blackfriday</code> Markdown engine. But that’s not the only example, do your own research. You’ll find so many more. Now its time to start your own. I know I am.</p>
</div>
</main>
<footer>
  <p><a href="/copyright/" id=copyright>© 2020 Rob Muhlestein</a></p>
</footer>
<script src="/assets/main.js"></script>
</body>
</html>