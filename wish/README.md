---
Title: I Wish ...
Subtitle: Maybe Someone Will Grant Them
---

Another reason to actually have a web site is simply a place to publicly
proclaim your wishes for yourself and the world. I suppose a wish is not
as weighty as a *goal* or a *todo* list. A wish is something that you
just want, not that you necessarily need to do yourself. In fact,
usually quite the opposite. You *wish* that someone else would do it
instead of forcing you to. Inevitably, I end up granting my own wishes
rather than complaining or sitting around waiting on someone else.

## Restream.io Command Line Tool with Chat

I *really* need finish the <https://restream.io> API command
integration. I'm hopeful that the Websockets API it provides does what
is necessary to replace my WeeChat TMUX pane on my live stream. Please
someone do it for me so I don't have to make it. Who am I kidding, I'll
have to make it myself or I'll never be satisfied. At least I could fork
it, I suppose. It just doesn't exist right now because no one cares
about the terminal anymore, too many low-brow graphics interface users
overwhelm the minority interests of we terminal people.

## Replacement for Find with a Good Query Language

The `find` command is an essential tool for system administration ---
particularly for disk usage and security audits --- but the syntax of
the arguments has always been ridiculously difficult to remember
requiring multiple references to the man page. 

Wouldn't it be cool if we came up with `find` replacement that has a
drop-dead simple query language (maybe even just SQL) that it read
either as a single string or passed into standard output, or just placed
in a file with a she-bang line pointing to it. It would be an *actual*
language for finding things on your system (no not like `awk` or `sed`).

One approach would be simply to create SQLite flat-database files and a
light-weight interface to them so that existing SQL tools could be used.
This is something that could be done rather quickly. By having different
SQLite files created queries could be made across databases to determine
changes in the file system.

## Nark, An Open-Source "Tripwire" Replacement Idea 

The original
[`tripwire`](https://github.com/Tripwire/tripwire-open-source) seems to
be alive and well. However, it looks like it could use an update and
perhaps a rewrite in a modern language (like Go or Rust). I'd also like
to focus on the reporting aspect above all and add both inbound and
outbound ports to the monitor to "nark" on (get it) stuff that is not
supposed to be on my system.

Another consideration would be the license change from GPLv2 to Apache.

What a great project this is for learning a specific language as well?
It's practical, uses tech and techniques that are relevant, and it's
relatively simple to implement.

## Watch With Me Application / Service

In order to comply with copyright law live streamers cannot do what they
would normally do, listen to copyrighted music and watch their favorite
videos and movies. This sucks because in order to live stream currently
you have to give all that up, but that is part of the fun of being a
coder, being able to jam out to Cake while banging on the keyboard.
Something needs to be created that keeps everyone watching the stream
informed about the URL and the location within the content enabling
stream viewers to be at exactly your same location so that people can
chat about it and comment on it like Mystery Science 3000. It might be
something as simple as a browser plugin and a service that can be
periodically polled by the plugin.

And another thing, I don't necessarily want to force those watching my
stream to listen to my music, or to watch the movies I'm watching. They
might now want to be distracted or perhaps they have their own music.
I've heard people say they actually *don't like* streams that have music
for that reason.

To date, most streamers will jam out with headphones without their
viewers knowing what they are listening to. That's fine, but it sort of
misses the point --- especially when wanting to view documentaries
together and such.

## Coder Coffee Shop Desktop App

I'd love to just be able to pull up an app on a secondary screen and see
everyone's screens while they are coding. The app could rotate through
random people and give them a 4x size for a bit just to highlight what
different people are working on. Those screens that don't change could
auto-minimize themselves so only people doing stuff would be on the
screen. Then we could add like a Discord chat room with it all. The one
solid requirement would be that it not crash constantly.

Even something that is just terminal based would be amazing, perhaps
something like what TMUX or Screen shared session do right now.

## Reformed Educational Systems Focused on Learning

Most public education systems (and most private ones as well) are not
[focused on learning](https://youtu.be/GFD37HvGx6k?t=181). They are focused on meeting quotas, perception
management, making money, pushing people through, and glorified
child-care. The most motivated teachers in such a system simply cannot
accomplish true learning in their students. When learning happens it
comes at the cost of dependency on the teacher and system instead of
producing true autodidactic individuals ready to face the world.

Teachers who have gone through the formal education to become one either
love or hate that when I say this. I'm convinced the science surrounding
education is seriously flawed and focused on the wrong outcomes. Ken
Robinson agrees. This is why I want nothing to do with traditional
academia and teacher certification. It turns well-intentioned teachers
into drones with their aspirations burned out by an objectively failing
system with upside-down priorities.

The path to reform means significantly changing the culture of
education. It means turning teachers into mentors and learning
facilitators, not subject-matter experts who have *actual* experience.
It means obliterating ego and promoting empathy in the learning
environment. It means having SMEs and facilitators working together. The
SMEs produce the content, challenges, and activities. The facilitators
organize learning lab environments where *the learner is in charge of their
own learning*. Responsibility for learning is placed on the learner from
the very beginning rather than creating an antagonistic teacher versus
student approach we have today. We need the courage to work
toward motivational skills and let those unwilling to learn simply
not learn, to hold them back for as long as it takes until they decide
on their own terms to begin and own their learning instead of trying to
*force* them to learn.

These techniques have been proven successful over and over again in
environments where the traditional systems gets completely out of the
way, from the groups of "lost learners" who are essentially thrown into
detention during school all day, to the "gifted and talented" deemed to
smart for the current system. Both of *those* systems flourish because
the teacher is not a teacher, but a motivator, a facilitator, and yes,
even a responsible friend. 

All of the plights of 2020 can be traced back to a lack of good
learning. Those who learn and love to learn discover empathy and
facilitators working in this way *must* be above all experts in
emotional intelligence and empathy, councilors more than teachers. Most
people who remember their "best" teacher will recall all of these
qualities in that person who operated this way within a seriously
broken system.

## Better PEG Grammar Notation and Tooling

*Update: I'm actually [making this](https://pegn.dev)
if you want to help.*

PEG ([Parser Expression Grammar](https://duck.com/lite?kae=t&q=Parser Expression Grammar)) is by far the most important advance
in language grammar development in the last 20 years (even though many
CS departments still don't even talk about it in their curriculum). The ecosystem
around PEG is fractured and full of over-engineered code generators that
are implementation language specific. What we need is a standardized
notation based on Bryan Ford's original (but incomplete) "example" in
his paper that started it all. Then we need to show that tooling can
exist --- including code generators --- that simply use the grammar and
don't intermix PEG with additional syntax to render in a specific
language. That way the same grammar files can be used to generate
parsers of different kinds in different languages. The same grammar file
could be used to generate a highly optimized parser within a single file
or another that uses very flexible AST approach and parse function
library.

PEG is particularly useful in computer science education not only to
create grammars, but to document existing ones and get the mind to
understand what syntax is as well as any specific syntax. Capturing an
existing language in PEG is a great educational exercise.

## Fast, One-Pass Parsable Markdown (Ezmark)

Markdown is amazing, but not even the CommonMark initiative has
recognized the value in creating a version of Markdown that can be
parsed in one pass instead of two. This is because of reference links
and other stuff in blocks at the bottom of a Markdown document that must
be parsed before anything above can be rendered that depends on them.

All the advances in Markdown --- mostly in Pandoc --- have been toward
become more flexible with adding extensions and providing more and more
additions to the syntax. This is entirely against the original goal of a
standard, simple language that is better than HTML than anyone can learn
in 20 minutes. We need a Markdown that enough people could use without a
lot of training not just because it is simple, but because HTML needs to
be replaced with some version of Markdown.

## A Decentralized Knowledge Network

This is one I have been working on for several years, something to
replace or supplement the world wide web that maintains the motivations
of the original web: knowledge exchange. It will leverage the
infrastructure of the web without depending on it. It will provide
localized content searching through semantic structuring, parsing, and
local caching. It won't even need a network. It will provide
digitally-signed attribution of authorship. It will use trust to improve
content discovery.

*Development is moving along slowly [here](https://gitlab.com/rwx.gg/kn).*

## Bash Shell Rewritten Under Better License

Seriously, Bash is so bloated and the code base so old and crusty that
the whole thing needs a serious rewrite under a license that will allow
it to ship on embedded hardware and such instead of Bash because of its
completely stupid [GPLv3 license](https://rwx.gg/views/gnuisdead).
(No [Zsh is *not*](https://rwx.gg/advice/dont/zsh) the answer.)

## Kick-Ass Educational Chemistry 3D Shooter

I've long wanted someone to take me up on the idea of creating a really
rated M educational chemistry shooter that does *smell* like a "for
education" game. It could be a full triple-A game that requires you to
prepare different ammo and weapons to fight an wildly different set of
monsters and mobs using the right stuff. There could be a chem lab where
you see the periodic table in 3D, play around with simulated
combinations of all the chemicals to learn about their properties and
more.

Hell just a realistic chem lab that taught about what happens when you
combine different chemicals would be amazing.

## Full YAML 1.2 and JQ Parser Command

The `jq` command is absolutely phenomenal. It just only exists for JSON.
There needs to be a `yq` as well that is *good* --- not that Python
PyYAML shit. It also needs to be written in Go, C, or Rust and maybe
have a lot of the unnecessary query stuff removed. It could be installed
as *both* `yq` and `jq` and detect how it is called in order to process
data differently. Fuck the old `jq` which never plans to support more
than the "JSON internal structures".

Such a tool would be so monumentally useful it might even put a serious
dent in the use of databases in general. Something that supported that
level of power, including the full remote files idea, would completely
dominate the flat-file data management space.

